# Define AWS provider
provider "aws" {
  region = "eu-north-1"
  
}

terraform {
  backend "s3" {
    bucket = "express-app-bucket"
    key    = "terraform.tfstate"  # The location within the bucket
    region = "eu-north-1"                  # Your desired AWS region
    encrypt = true      
    
  }
}

# EC2 Key Pair
resource "aws_key_pair" "deployer_key" {
  key_name   = "github-deploy-key"
  public_key = file("./id_rsa.pub") # Adjust this to point to your actual public key
}

# Security Group
resource "aws_security_group" "express_sg" {
  name        = "express-app-sg"
  description = "Allow HTTP and SSH"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# EC2 Instance
resource "aws_instance" "express_instance" {
  ami           = "ami-05edb7c94b324f73c" 
  instance_type = "t3.micro"
  key_name      = aws_key_pair.deployer_key.key_name

  security_groups = [
    aws_security_group.express_sg.name,
  ]

  tags = {
    Name = "express-app-instance"
  }
}

output "instance_public_ip" {
  value = aws_instance.express_instance.public_ip
}
