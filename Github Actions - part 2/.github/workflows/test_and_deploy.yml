name: Test & Deploy 

on:
  workflow_dispatch:
    inputs:
      instance_ip:
        description: 'EC2 Instance Public IP'
        required: true
        type: string
      port_number:
        description: 'Port number'
        required: true
        type: int
jobs:
  test:
    runs-on: ubuntu-24.04
    steps:
      - name: Checkout code
        uses: actions/checkout@v3

      - name: Set up Node.js
        uses: actions/setup-node@v3
        with:
          node-version: 18

      - name: Cache node_modules
        uses: actions/cache@v3
        with:
          path: node_modules
          key: ${{ runner.os }}-node-${{ hashFiles('**/package-lock.json') }}
          restore-keys: |
            ${{ runner.os }}-node-

      - name: Install dependencies
        run: npm ci

      - name: Test the code
        run: npm test

  deploy:
    runs-on: ubuntu-24.04
    needs: test
    steps:
      - name: Checkout code
        uses: actions/checkout@v3

      - name: Setup SSH private key for deployment
        run: |
          echo "${{ secrets.AWS_SSH_EXPRESS_PRIV_KEY }}" > private_key.pem
          chmod 600 private_key.pem
          ls -la

      - name: Copy files to EC2 instance using SCP
        run: |
          # Export EC2 instance IP from user input
          INSTANCE_IP=${{ github.event.inputs.instance_ip }}
          
          # Copy the code to the EC2 instance using SCP
          scp -o StrictHostKeyChecking=no -i private_key.pem -r . ec2-user@$INSTANCE_IP:/home/ec2-user/app

      - name: Deploy on EC2 instance
        run: |
          INSTANCE_IP=${{ github.event.inputs.instance_ip }}
          PORT=${{ github.event.inputs.port_number }}  # Port passed as input

          # SSH into EC2 with the environment variable
          ssh -t -o StrictHostKeyChecking=no -i private_key.pem ec2-user@$INSTANCE_IP << EOF
            set -e  # Fail on any error

            # Update the system and install Node.js (Amazon Linux 2023)
            sudo yum update -y
            curl -sL https://rpm.nodesource.com/setup_18.x | sudo bash -
            sudo yum install -y nodejs

            # Navigate to the app directory
            cd /home/ec2-user/app

            # Install dependencies
            npm install

            # Stop any existing application
            sudo pkill -f "node app/index.js" || true  # Ensure previous app is stopped

            # Export the environment variable
            export PORT=$PORT

            # Start the application with sudo and fully detach
            echo "Starting application on port \$PORT"
            nohup sudo PORT=\$PORT npm start > app.log 2>&1 & disown

            # Wait briefly to ensure the application starts
            sleep 5

            # Verify the application is running
            if ! sudo lsof -i :\$PORT; then
              echo "Application failed to start on port \$PORT"
              exit 1
            else
              echo "Application is running successfully on port \$PORT"
            fi

            # Cleanly exit the SSH session
            exit 0
          EOF

      - name: Clean up SSH key
        run: rm -f private_key.pem