// app.js
const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send(process.env.RESPONSE_MESSAGE || 'Hello, World!');
});

const PORT = process.env.PORT || 3000;

if (require.main === module) {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}

process.on('SIGTERM', () => {
  console.log('Shutting down server...');
  server.close(() => {
    console.log('Server closed');
    process.exit(0);
  });
});

module.exports = { app, PORT }; 
