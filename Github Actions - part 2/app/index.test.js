const request = require('supertest');
const { app } = require('.');  

let server; 

beforeAll(() => {
  server = app.listen(0); 
});

afterAll(() => {
  server.close();
});

describe('Main Endpoint', () => {
  it('should return "Hello, World!" if RESPONSE_MESSAGE is not set', async () => {
    const response = await request(server).get('/');
    expect(response.status).toBe(200); 
    expect(response.text).toBe('Hello, World!');  // Check that the response body is "Hello, World!"
  });

  it('should return custom message if RESPONSE_MESSAGE is set', async () => {
    process.env.RESPONSE_MESSAGE = 'Custom Message';  
    const response = await request(server).get('/');
    expect(response.status).toBe(200);  
    expect(response.text).toBe('Custom Message'); 
  });
});
