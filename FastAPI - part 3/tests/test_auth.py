from httpx import Response
from fastapi import status
from app import app
from pymongo.database import Database
from typing import Any, Callable
import os
from app.database.connector import Connector
from tests.conftest.conftest import client, app, inmemory_database_creation_function, API_AUTHENTICATION_PREFIX


envs: dict[str, str] = {
    'JWT_ACCESS_TOKEN_SECRET_KEY': 'accesstokenkey',
    'JWT_ACCESS_TOKEN_EXPIRE_MINUTES': '10080',
    'JWT_TOKEN_ALG': 'HS256',
}


def test_given_existing_account_when_logging_in_then_response_parameters_are_ok(
    inmemory_database_creation_function: Callable[[], Database[Any]],
    monkeypatch
) -> None: 
    # Update ENV variables
    monkeypatch.setattr(os, 'environ', envs)
    # Mock DB
    app.dependency_overrides[Connector.get_db] = inmemory_database_creation_function

    # Given
    user_data: dict[str, str] = {
        "email": "aaa@aaa.com",
        "password": "aaa@aaa.com",
        "role": "user"
    }
    
    # When
    response: Response = client.post(API_AUTHENTICATION_PREFIX+"/login", json=user_data)
    response_json = response.json()

    # Then
    assert response.status_code == status.HTTP_200_OK
    assert "access_token" in response_json
    assert response.headers["Token-Type"] == "Bearer"


def test_given_proper_user_when_registering_the_user_then_created_request_is_returned(
    inmemory_database_creation_function: Callable[[], Database[Any]],
) -> None: 
    # Mock DB
    app.dependency_overrides[Connector.get_db] = inmemory_database_creation_function
   
    # Given
    user_data: dict[str, str] = {
        "email": "test@test.com",
        "password": "password123",
        "role":"user"
    }
    # When
    response: Response = client.post(API_AUTHENTICATION_PREFIX+"/register", json=user_data)

    # Then
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {"email": "test@test.com", "role":"user"}
