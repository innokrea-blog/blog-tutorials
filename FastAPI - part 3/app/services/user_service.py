import re
from app.repositories.user_repository import UserRepository
from app.models.models import User
from hashlib import sha256
from app.schemas.schemas import *
from app.exceptions.exceptions import DuplicateUserException, InvalidEmailFormatException, InvalidCredentialsException
from typing import Union

email_regex = r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b"

def is_email_valid(email: str) -> bool:
    return bool(re.fullmatch(email_regex, email))

class UserService:
    def __init__(self, repository:UserRepository) -> None:
        self.repository: UserRepository = repository

    def create_user(self, data:NewUserRequest, role:str) -> User:
        email :str = data.email
        password : str = data.password
        if not is_email_valid(data.email):
            raise InvalidEmailFormatException()
        if self.repository.find_user_by_email(email):
            raise DuplicateUserException()
            
        password_hash: str = sha256(password.encode('utf-8')).hexdigest()
        user:User = User(email=email,password_hash=password_hash, role=role)   
        user = self.repository.add_user(user)
        return user
    
    def check_user_credentials(self, email:str, password:str) -> User:
        if not is_email_valid(email):
            raise InvalidEmailFormatException()
        
        user:Union[User,None] = self.repository.find_user_by_email(email)        
        if not user or sha256(password.encode('utf-8')).hexdigest() != user.password_hash:
            raise InvalidCredentialsException()

        return user

    def check_user_exists(self,email) -> User:     
        if not is_email_valid(email):
            raise InvalidEmailFormatException()
        
        user:Union[User,None] = self.repository.find_user_by_email(email)
        if not user:
            raise InvalidCredentialsException()    
        return user
