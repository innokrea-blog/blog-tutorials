from typing import Union, Any
import os
from app.repositories.user_repository import UserRepository
from datetime import datetime, timedelta, timezone
from app.models.models import User
import jwt

class TokenService:
    def __init__(self, user_repository: UserRepository) -> None:
        self.user_repository: UserRepository = user_repository        
        self.jwt_access_token_secret_key: Union[str, None]  = os.getenv('JWT_ACCESS_TOKEN_SECRET_KEY')
        self.jwt_access_token_expire_minutes: int = int(os.getenv('JWT_ACCESS_TOKEN_EXPIRE_MINUTES', 7 * 24 * 60))
        self.jwt_token_alg: Union[str, None] = os.getenv('JWT_TOKEN_ALG')
 
    def generate_access_token(self, user_data: User) -> str:
        expire: datetime = datetime.now(timezone.utc) + timedelta(minutes=self.jwt_access_token_expire_minutes)
        data_to_encode: dict[str, Any] = {"email": user_data.email, "role": user_data.role, "exp": expire}
        encoded_jwt: str = jwt.encode(data_to_encode, str(self.jwt_access_token_secret_key), str(self.jwt_token_alg))
        return encoded_jwt
