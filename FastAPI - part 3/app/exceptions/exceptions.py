from fastapi import status, HTTPException
from fastapi.responses import JSONResponse
from fastapi import Request

# definitions

class DuplicateUserException(HTTPException):
    def __init__(self, status_code: int = status.HTTP_400_BAD_REQUEST, detail: str = "This e-mail is not acceptable"):
        super().__init__(status_code=status_code, detail=detail)

class InvalidEmailFormatException(HTTPException):
    def __init__(self, status_code: int = status.HTTP_400_BAD_REQUEST, detail: str = "This e-mail is not acceptable"):
        super().__init__(status_code=status_code, detail=detail)

class InvalidCredentialsException(HTTPException):
    def __init__(self, status_code: int = status.HTTP_401_UNAUTHORIZED, detail: str = "Incorrect e-mail or password"):
        super().__init__(status_code=status_code, detail=detail)

# handlers

async def handle_duplicate_user_exception(request: Request, exc: DuplicateUserException):
    return JSONResponse(
        status_code=exc.status_code,
        content={'detail': exc.detail}
    )

async def handle_invalid_email_format_exception(request: Request, exc: InvalidEmailFormatException):
    return JSONResponse(
        status_code=exc.status_code,
        content={'detail': exc.detail}
    )

async def handle_invalid_credentials_exception(request: Request, exc: InvalidCredentialsException):
    return JSONResponse(   
        status_code=exc.status_code,
        content={'detail': exc.detail}
    )
