from fastapi import APIRouter, Depends, status, Response
import os
from app.schemas.schemas import NewUserRequest, NewUserResponse, UserLoginRequest, UserLoginResponse
from app.services.user_service import UserService
from app.services.token_service import TokenService
from app.models.models import User
from app.dependencies.dependencies import get_user_service, get_token_service

router = APIRouter(
    prefix=os.getenv('API_AUTHENTICATION_PREFIX','/api'),
    tags=['user']
)

@router.post("/register", response_model=NewUserResponse, status_code=status.HTTP_201_CREATED)
def register(data: NewUserRequest, service: UserService = Depends(get_user_service)):   
    user: User = service.create_user(data, "user")
    return NewUserResponse(email=user.email, role=user.role)

@router.post("/login",response_model=UserLoginResponse, status_code=status.HTTP_200_OK)
def login(data: UserLoginRequest, response: Response, user_service: UserService = Depends(get_user_service), token_service: TokenService = Depends(get_token_service)):
    user: User = user_service.check_user_credentials(data.email, data.password)
     
    access_token: str = token_service.generate_access_token(user) 
    response_model: UserLoginResponse = UserLoginResponse(access_token=access_token)
    response.headers.append("Token-Type", "Bearer")
     
    return response_model

