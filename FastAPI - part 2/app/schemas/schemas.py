from pydantic import BaseModel

class NewUserRequest(BaseModel):
    email: str
    password: str

class UserLoginRequest(BaseModel):
    email: str
    password: str
    
class NewUserResponse(BaseModel):
    email: str
    role: str

class UserLoginResponse(BaseModel):
    access_token: str
