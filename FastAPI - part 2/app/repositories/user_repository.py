from pymongo.collection import Collection
from pymongo.database import Database
from pymongo.results import InsertOneResult
from app.models.models import User
from typing import Any, Dict, Union

class UserRepository:
    def __init__(self, database: Database) -> None:
        self.database: Database = database
        self.users_collection: Collection = self.database['users']

    def add_user(self, user: User) -> User:
        user_dict: Dict[str, Any] = user.model_dump()
        insertion_result: InsertOneResult = self.users_collection.insert_one(user_dict)
        user._id = insertion_result.inserted_id
        return user

    def find_user_by_email(self, email: str) -> Union[User, None]:
        user_data: Union[Dict[str, Any], None] = self.users_collection.find_one({"email": email})
        if not user_data:
            return None
        return User(**user_data)
