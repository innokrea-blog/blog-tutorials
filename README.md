# Blog Tutorials


Welcome to the official repository for the code used in articles published on our [blog](https://www.innokrea.com/blog/)!

Here you will find the source code files that are freely available and can be downloaded. This repository aims to facilitate the learning and practical use of the knowledge presented in our articles.


## Index of articles and associated files:

1. Vite, React and Docker -> [Dockerizing the frontend - do it right with React.js + Vite](https://www.innokrea.com/dockerizing-the-frontend-do-it-right-with-react-js-vite/)
2. FastAPI - part 1 -> [FastAPI - how to write a simple REST API in Python? - part 1](https://www.innokrea.com/fastapi-how-to-write-a-simple-rest-api-in-python-part-1/)
3. FastAPI - part 2 -> [FastAPI - or how to write a simple REST API in Python? - Part 2](https://www.innokrea.com/fastapi-or-how-to-write-a-simple-rest-api-in-python-part-2/)
4. FastAPI - part 3 -> [FastAPI - How to Build a Simple REST API in Python? - Part 3](https://www.innokrea.com/fastapi-how-to-build-a-simple-rest-api-in-python-part-3/)
5. Design Patterns - part 1 -> [Design Patterns - part 1](https://www.innokrea.com/design-patterns-part-1/)
6. Design Patterns - part 2 -> [Design Patterns - part 2](https://www.innokrea.com/design-patterns-part-2/)
7. Github Actions - part 1 -> [CI/CD - How to Use GitHub Actions to Build Pipelines? - Part 1](https://www.innokrea.com/ci-cd-how-to-use-github-actions-to-build-pipelines-part-1/)
8. Github Actions - part 2 -> [CI/CD + Terraform – How to Deploy Your Application on AWS? – Part 2](https://www.innokrea.com/ci-cd-terraform-how-to-deploy-your-application-on-aws-part-2/)