
public class Client {
    public static void main(String[] args) {

        System.out.print("Welcome to the shop! \n \n");
        System.out.print("<<<Online order processing test>>>\n");

        OrderProcessTemplate onlineOrder = new OnlineOrderProcess();
        onlineOrder.processOrder();
        System.out.println("\n------------------------------------------------------\n");
        System.out.print("<<<Store order processing test with Stripe payment strategy>>>\n");

        OrderProcessTemplate storeOrder = new StoreOrderProcess();
        storeOrder.processOrder();
    }
}