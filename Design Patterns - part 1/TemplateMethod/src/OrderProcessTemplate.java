

public abstract class OrderProcessTemplate {
    public OrderProcessTemplate() {
    }

    public final void processOrder() {
        selectProduct();
        if (isGift()) {
            wrapGift();
        }
        deliver();
    }

    protected abstract void selectProduct();
    protected abstract void deliver();


    protected boolean isGift() {
        return false;
    }

    private void wrapGift() {
        System.out.println("Gift wrapping completed.");
    }
}