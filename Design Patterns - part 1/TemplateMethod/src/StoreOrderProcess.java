

public class StoreOrderProcess extends OrderProcessTemplate {

    public StoreOrderProcess() {
        super();
    }

    @Override
        protected void selectProduct() {
            System.out.println("Selecting product from the inventory.");
        }

        @Override
        protected void deliver() {
            System.out.println("Customer will pick up the product from the store.");
        }

}
