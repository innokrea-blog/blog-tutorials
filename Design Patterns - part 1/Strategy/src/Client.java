
public class Client {
    public static void main(String[] args) {

        System.out.print("Welcome to the shop! \n \n");
        IPaymentStrategy paymentStrategy1 = new PaypalPayment();
        IPaymentStrategy paymentStrategy2 = new StripePayment();
        Store onlineStore = new Store(paymentStrategy1);
        onlineStore.executePayment();

        onlineStore.setPaymentStrategy(paymentStrategy2);
        onlineStore.executePayment();
    }
}