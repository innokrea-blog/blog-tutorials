public class StripePayment implements  IPaymentStrategy{
    @Override
    public void pay(Double amount, String toAccountId) {
        // Payment logic implementation
        System.out.println("Stripe payment:  "+amount+" paid to "+toAccountId);
    }
}
