public class Store {
    // Online store class
    Double cartValueAmount = 30.0;
    String shopAccountId = "123321";
    IPaymentStrategy paymentStrategy;
    public Store(IPaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public void setPaymentStrategy(IPaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public void executePayment() {
        System.out.println("Executing payment strategy with class: " + paymentStrategy.getClass().getName());
        this.paymentStrategy.pay(this.cartValueAmount, this.shopAccountId);
        System.out.println("Payment successful!\n");
    }
}
