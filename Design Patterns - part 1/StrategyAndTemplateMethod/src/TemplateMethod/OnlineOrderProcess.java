package TemplateMethod;

import Strategy.IPaymentStrategy;

public class OnlineOrderProcess extends OrderProcessTemplate {

    public OnlineOrderProcess(IPaymentStrategy paymentStrategy) {
        super(paymentStrategy);
    }
    @Override
    protected void selectProduct() {
        System.out.println("Selecting product from online catalog.");
    }

    @Override
    protected void deliver() {
        System.out.println("Delivering product to the customer's address.");
    }

    @Override
    protected boolean isGift() {
        return true;
    }
}
