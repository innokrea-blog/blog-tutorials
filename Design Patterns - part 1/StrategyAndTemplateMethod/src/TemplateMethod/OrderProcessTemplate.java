package TemplateMethod;

import Strategy.IPaymentStrategy;

public abstract class OrderProcessTemplate {
    protected IPaymentStrategy paymentStrategy;
    Double cartValueAmount = 30.0;
    String shopAccountId = "123321";

    public OrderProcessTemplate(IPaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public final void processOrder() {
        selectProduct();
        executePayment();
        if (isGift()) {
            wrapGift();
        }
        deliver();
    }

    protected abstract void selectProduct();
    protected abstract void deliver();

    protected void executePayment() {
        System.out.println("Executing payment strategy with class: " + paymentStrategy.getClass().getName());
        this.paymentStrategy.pay(this.cartValueAmount, this.shopAccountId);
        System.out.println("Payment successful!");
    }

    protected boolean isGift() {
        return false;
    }

    private void wrapGift() {
        System.out.println("Gift wrapping completed.");
    }
}