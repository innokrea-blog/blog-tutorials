package Strategy;

public interface IPaymentStrategy {
    void pay(Double amount, String toAccountId);
}
