package Strategy;

public class PaypalPayment implements  IPaymentStrategy{
    @Override
    public void pay(Double amount, String toAccountId) {
        // Payment logic implementation
        System.out.println("Paypal payment: "+amount+" paid to "+toAccountId);
    }
}
