import Strategy.IPaymentStrategy;
import Strategy.PaypalPayment;
import Strategy.StripePayment;
import TemplateMethod.OnlineOrderProcess;
import TemplateMethod.OrderProcessTemplate;
import TemplateMethod.StoreOrderProcess;

public class Client {
    public static void main(String[] args) {

        System.out.print("Welcome to the shop! \n \n");
        System.out.print("<<<Online order processing test with Paypal payment strategy>>>\n");
        IPaymentStrategy paymentStrategy1 = new PaypalPayment();
        OrderProcessTemplate onlineOrder = new OnlineOrderProcess(paymentStrategy1);
        onlineOrder.processOrder();
        System.out.println("\n------------------------------------------------------\n");
        System.out.print("<<<Store order processing test with Stripe payment strategy>>>\n");
        IPaymentStrategy paymentStrategy2 = new StripePayment();
        OrderProcessTemplate storeOrder = new StoreOrderProcess(paymentStrategy2);
        storeOrder.processOrder();



    }
}