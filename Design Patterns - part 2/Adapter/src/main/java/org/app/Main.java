package org.app;

import org.json.*;

public class Main {
    public static void main(String[] args) {
        LegacySensorDriver legacyDriver = new LegacySensorDriver();
        LegacyToJsonSensorAdapter adapter = new LegacyToJsonSensorAdapter(legacyDriver);
        JSONObject sensorDataJson = adapter.parseSensorDataToJson();
        System.out.println(sensorDataJson.toString(2));
    }
}