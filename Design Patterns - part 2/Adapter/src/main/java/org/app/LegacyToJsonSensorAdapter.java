package org.app;
import org.json.JSONObject;

public class LegacyToJsonSensorAdapter implements SensorDataJsonParser {
    private final LegacySensorDriver legacySensorDriver;

    public LegacyToJsonSensorAdapter(LegacySensorDriver legacySensorDriver) {
        this.legacySensorDriver = legacySensorDriver;
    }

    @Override
    public JSONObject parseSensorDataToJson() {
        String rawData = legacySensorDriver.getSensorData();

        String[] dataParts = rawData.split(",");
        double temperature = Double.parseDouble(dataParts[0]);
        double humidity = Double.parseDouble(dataParts[1]);

        JSONObject sensorDataJson = new JSONObject();
        sensorDataJson.put("temperature", temperature);
        sensorDataJson.put("humidity", humidity);

        return sensorDataJson;
    }
}
