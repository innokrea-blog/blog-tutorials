package org.app;
import java.util.Random;

public class LegacySensorDriver {

    public String getSensorData() {
        Random random = new Random();
        double temperature = 15 + random.nextDouble() * 20;
        double humidity = 20 + random.nextDouble() * 20;

        return String.format("%.1f,%.1f", temperature, humidity);
    }
}
