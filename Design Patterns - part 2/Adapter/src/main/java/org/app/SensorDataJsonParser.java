package org.app;

import org.json.JSONObject;

public interface SensorDataJsonParser {
    JSONObject parseSensorDataToJson();
}