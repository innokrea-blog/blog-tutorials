package org.app;

public class Main {
    public static void main(String[] args) {
        Originator originator = new Originator();
        Caretaker careTaker = new Caretaker(originator);


        originator.setText("test1");
        careTaker.save();
        System.out.println(originator.getText());

        originator.setText("test2");
        careTaker.save();
        System.out.println(originator.getText());

        originator.setText("test3");
        System.out.println(originator.getText());
        careTaker.undo();
        System.out.println(originator.getText());
    }
}