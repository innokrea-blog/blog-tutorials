package org.app;

import java.util.Stack;

public class Caretaker {

    private final Stack<IMemento> history = new Stack<>();
    private final Originator originator;

    public Caretaker(Originator originator) {
        this.originator = originator;
    }

    public void save() {
        history.push(this.originator.saveToMemento());
    }

    public void undo() {
        if (!history.isEmpty()) {
            this.originator.restoreFromMemento(history.pop());
            System.out.println("Restoring the state!");
        }
        else {
            System.out.println("No previous state");
        }
    }
}
