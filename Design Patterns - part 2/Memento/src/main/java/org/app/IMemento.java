package org.app;

public interface IMemento {
    public String getSavedText();
}
