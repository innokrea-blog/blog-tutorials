package org.app;

public class Originator {
    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public IMemento saveToMemento() {
        return new Memento(text);
    }

    public void restoreFromMemento(IMemento memento) {

        if (!(memento instanceof Memento))
        {
            throw new IllegalArgumentException("Unknown memento class: " + memento.getClass());
        }
        this.text = memento.getSavedText();
    }

    private static class Memento implements IMemento{
        private String savedText;

        public Memento(String text) {
            this.savedText = text;
        }

        public String getSavedText() {
            return savedText;
        }
    }
}